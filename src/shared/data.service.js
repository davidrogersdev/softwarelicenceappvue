import * as axios from "axios";

const baseUrl = "https://localhost:5001";
const apiVersion = "v1.0";
const fullBase = `${baseUrl}/${apiVersion}`;

const getAllocationsDetails = () => {

    return axios({
        method: "GET",
        url: `${fullBase}/LicenceAllocation/GetLicenceAllocationsAndDetails`
    });
};

export const dataService = {
    getAllocationsDetails: getAllocationsDetails
};
